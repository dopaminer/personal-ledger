import os, sys

from PyQt6.QtCore import (
	QDate,
	QSize,
	Qt
	)
from PyQt6.QtGui import (
	QAction,
	QIcon,
	QKeySequence,
	QStandardItem,
	QStandardItemModel,
	)
from PyQt6.QtWidgets import (
	QApplication,
	QButtonGroup,
	QCheckBox,
	QComboBox,
	QDateEdit,
	QDialog,
	QDialogButtonBox,
	QGroupBox,
	QHBoxLayout,
	QLabel,
	QLineEdit,
	QListView,
	QMainWindow,
	QMessageBox,
	QPlainTextEdit,
	QPushButton,
	QRadioButton,
	QSpinBox,
	QToolBar,
	QVBoxLayout,
	QWidget,
	)


class AddPersonDialog(QDialog):
	"""Window with fields to input person info and save it."""
	
	def __init__(self, parent=None):
		super().__init__(parent)
		self.setWindowTitle("Add Person")
		
		# Create name_field and its layout
		name_field = QWidget()
		name_field_layout = QHBoxLayout()
		# Create name QLabel and QLineEdit fields and add to name_field_layout
		name_label = QLabel("Name: ")
		self.name_line_edit = QLineEdit()
		self.name_line_edit.setMaxLength(20)
		name_field_layout.addWidget(name_label)
		name_field_layout.addWidget(self.name_line_edit)
		# Set layout for name field
		name_field.setLayout(name_field_layout)
	
		# Create notes_field and its layout
		notes_field = QWidget()
		notes_field_layout = QHBoxLayout()
		# Create notes QLabel and QPlainTextEdit fields and add to notes_field_layout
		notes_label = QLabel("Notes: ")
		self.notes_text_edit = QPlainTextEdit()
		notes_field_layout.addWidget(notes_label)
		notes_field_layout.addWidget(self.notes_text_edit)
		# Set layout for notes field
		notes_field.setLayout(notes_field_layout)

		# Create Save and Cancel buttons
		buttons = (
			QDialogButtonBox.StandardButton.Save |
			QDialogButtonBox.StandardButton.Cancel
			)
		
		# Create buttonBox and connect accept and reject signals
		self.buttonBox = QDialogButtonBox(buttons)
		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)
		
		# Create overall dialog layout
		dialog_layout = QVBoxLayout()

		# Add name and notes fields to dialog layout
		dialog_layout.addWidget(name_field)
		dialog_layout.addWidget(notes_field)
		dialog_layout.addWidget(self.buttonBox)
		
		# Set overall dialog layout
		self.setLayout(dialog_layout)


class EditPersonDialog(QDialog):
	"""Window to edit person's fields."""

	def __init__(self, main_window, name, notes, parent=None):
		super().__init__(parent)

		self.setWindowTitle("Edit Person")

		# Create name_field and its layout
		name_field = QWidget()
		name_field_layout = QHBoxLayout()
		# Create name QLabel and QLineEdit fields and add to name_field_layout
		name_label = QLabel("Name: ")
		self.name_line_edit = QLineEdit(name) # Populate with person's name
		self.name_line_edit.setMaxLength(20)
		name_field_layout.addWidget(name_label)
		name_field_layout.addWidget(self.name_line_edit)
		# Set layout for name field
		name_field.setLayout(name_field_layout)
	
		# Create notes_field and its layout
		notes_field = QWidget()
		notes_field_layout = QHBoxLayout()
		# Create notes QLabel and QPlainTextEdit fields and add to notes_field_layout
		notes_label = QLabel("Notes: ")
		self.notes_text_edit = QPlainTextEdit(notes) # Populate with notes
		notes_field_layout.addWidget(notes_label)
		notes_field_layout.addWidget(self.notes_text_edit)
		# Set layout for notes field
		notes_field.setLayout(notes_field_layout)

		# Create Save and Cancel buttons
		buttons = (
			QDialogButtonBox.StandardButton.Save |
			QDialogButtonBox.StandardButton.Cancel
			)
		# Create button box and add buttons
		self.buttonBox = QDialogButtonBox(buttons)
		# Connect accept and reject signals
		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)

		# Create overall dialog layout
		dialog_layout = QVBoxLayout()

		# Add name and notes fields to dialog layout
		dialog_layout.addWidget(name_field)
		dialog_layout.addWidget(notes_field)
		dialog_layout.addWidget(self.buttonBox)
		
		# Set overall dialog layout
		self.setLayout(dialog_layout)


class DeletePersonDialog(QDialog):
	"""Window with checkboxes to select people to delete."""

	def __init__(self, main_window, parent=None):
		super().__init__(parent)

		self.setWindowTitle("Delete Person")
				
		# Create button box, then add "DELETE" and "Cancel" buttons
		self.buttonBox = QDialogButtonBox()
		self.buttonBox.addButton(
			"DELETE",
			QDialogButtonBox.ButtonRole.AcceptRole
			)
		self.buttonBox.addButton(QDialogButtonBox.StandardButton.Cancel)

		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)

		self.layout = QVBoxLayout()
		self.message = QLabel("Select people to delete:")
		self.layout.addWidget(self.message)

		# Create alphabetized list of people from db
		people = []
		for person, info in main_window.db.people.items():
			name = info['name'].title()
			people.append(name)
		people.sort()
		
		# Create people checklist
		self.people_checkboxes = []
		for name in people:
			name_checkbox = QCheckBox(name)
			self.layout.addWidget(name_checkbox)
			self.people_checkboxes.append(name_checkbox)

		# Create warning label and add to layout
		self.warning = QLabel(
			"The selected people will be PERMANENTLY"
			"\ndeleted along with ALL of their transactions."
			)
		self.layout.addWidget(self.warning)
		
		# Add buttonBox to layout
		self.layout.addWidget(self.buttonBox)
		
		# Set overall layout
		self.setLayout(self.layout)


class AddTransactionDialog(QDialog):
	"""Window with fields to input transaction info and save it."""
	
	def __init__(self, main_window, person_id, parent=None):
		super().__init__(parent)
		self.setWindowTitle("Add Transaction")
		self.setMinimumSize(400, 200)

		# Transactions expected in the following format:
		# 	{id_number: {
		#		"person": person,
		#		"date": date,
		#		"amount": amount,
		#		"notes": notes,
		#	}}

		### Person Field ###
		# Create person_field and its layout
		person_field = QWidget()
		person_field_layout = QHBoxLayout()
		# Create person QLabel and QComboBox fields
		# and add them to person_field_layout
		person_label = QLabel("Person: ")
		self.person_combo_box = QComboBox()
		person_list = []
		for person, info in main_window.db.people.items():
			person_list.append(info['name'].title())
		person_list.sort()
		self.person_combo_box.insertItems(0, person_list)
		# Set person_combo_box initial selection to currently selected person
		person_count = self.person_combo_box.count()
		selected_person = main_window.db.people[person_id]['name']
		for index in range(person_count):
			person = self.person_combo_box.itemText(index).lower()
			if person == selected_person:
				self.person_combo_box.setCurrentIndex(index)
		# Add items to person_field_layout
		person_field_layout.addWidget(person_label)
		person_field_layout.addWidget(self.person_combo_box)
		# Set layout for person_field
		person_field.setLayout(person_field_layout)

		### Date Field ###
		# Create date_field and its layout
		date_field = QWidget()
		date_field_layout = QHBoxLayout()
		# Create date QLabel and QDateEdit fields
		# and add them to date_field_layout
		date_label = QLabel("Date: ")
		self.date_edit = QDateEdit()
		current_date = QDate.currentDate()
		self.date_edit.setDate(current_date)
		self.date_edit.setCalendarPopup(True)
		date_field_layout.addWidget(date_label)
		date_field_layout.addWidget(self.date_edit)
		# Set layout for date_field
		date_field.setLayout(date_field_layout)

		### Amount Field ####
		# Create amount_field and its layout
		amount_field = QWidget()
		amount_field_layout = QHBoxLayout()
		# Create amount QLabel and QSpinBox fields
		# and add them to amount_field_layout
		amount_label = QLabel("Amount: ")
		self.spin_box = QSpinBox()
		self.spin_box.setPrefix("$")
		self.spin_box.setMinimum(-10_000)
		self.spin_box.setMaximum(10_000)
		amount_field_layout.addWidget(amount_label)
		amount_field_layout.addWidget(self.spin_box)
		# Set layout for amount_field
		amount_field.setLayout(amount_field_layout)

		### Note Field ###
		# Create note field and its layout
		note_field = QWidget()
		note_field_layout = QHBoxLayout()
		# Create note QLabel and QLineEdit fields
		# and add them to note_field_layout
		note_label = QLabel("Note: ")
		self.line_edit = QLineEdit()
		self.line_edit.setMaxLength(50)
		note_field_layout.addWidget(note_label)
		note_field_layout.addWidget(self.line_edit)
		# Set layout for note_field
		note_field.setLayout(note_field_layout)

		# Create Save and Cancel buttons
		buttons = (
			QDialogButtonBox.StandardButton.Save |
			QDialogButtonBox.StandardButton.Cancel
			)
		
		# Create buttonBox and connect accept and reject signals
		self.buttonBox = QDialogButtonBox(buttons)
		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)
		
		# Create overall dialog layout
		dialog_layout = QVBoxLayout()

		# Add name and notes fields to dialog layout
		dialog_layout.addWidget(person_field)
		dialog_layout.addWidget(date_field)
		dialog_layout.addWidget(amount_field)
		dialog_layout.addWidget(note_field)
		dialog_layout.addWidget(self.buttonBox)
		
		# Set overall dialog layout
		self.setLayout(dialog_layout)


class EditTransactionDialog(QDialog):
	"""Window with fields to edit existing transaction info and save it."""
	
	def __init__(self, main_window, trx_id, parent=None):
		super().__init__(parent)
		self.setWindowTitle("Edit Transaction")
		self.setMinimumSize(400, 200)
		self.mw = main_window
		trx = self.mw.db.transactions[trx_id]

		#=== Person Field ===#
		# Create person_field and its layout
		person_field = QWidget()
		person_field_layout = QHBoxLayout()
		# Create person QLabel and QComboBox fields
		# and add them to person_field_layout
		person_label = QLabel("Person: ")
		self.person_combo_box = QComboBox()
		person_list = []
		for person, info in main_window.db.people.items():
			person_list.append(info['name'].title())
		person_list.sort()
		self.person_combo_box.insertItems(0, person_list)
		# Set initial selection to transaction person from database
		person_count = self.person_combo_box.count()
		for index in range(person_count):
			person = self.person_combo_box.itemText(index).lower()
			person_id = self.mw.db.get_person_id(person)
			if person_id == trx['person_id']:
				self.person_combo_box.setCurrentIndex(index)

		person_field_layout.addWidget(person_label)
		person_field_layout.addWidget(self.person_combo_box)
		# Set layout for person_field
		person_field.setLayout(person_field_layout)

		#=== Date Field ===#
		# Create date_field and its layout
		date_field = QWidget()
		date_field_layout = QHBoxLayout()
		# Create date QLabel and QDateEdit fields
		# and add them to date_field_layout
		date_label = QLabel("Date: ")
		self.date_edit = QDateEdit()
		trx_date_string = trx['date']
		trx_q_date = QDate.fromString(trx_date_string, Qt.DateFormat.ISODate)
		self.date_edit.setDate(trx_q_date)
		self.date_edit.setCalendarPopup(True)
		date_field_layout.addWidget(date_label)
		date_field_layout.addWidget(self.date_edit)
		# Set layout for date_field
		date_field.setLayout(date_field_layout)

		#=== Amount Field ===#
		# Create amount_field and its layout
		amount_field = QWidget()
		amount_field_layout = QHBoxLayout()
		# Create amount QLabel and QSpinBox fields
		# and add them to amount_field_layout
		amount_label = QLabel("Amount: ")
		self.spin_box = QSpinBox()
		self.spin_box.setPrefix("$")
		self.spin_box.setMinimum(-10_000)
		self.spin_box.setMaximum(10_000)
		self.spin_box.setValue(trx['amount'])
		amount_field_layout.addWidget(amount_label)
		amount_field_layout.addWidget(self.spin_box)
		# Set layout for amount_field
		amount_field.setLayout(amount_field_layout)

		#=== Note Field ===#
		# Create note field and its layout
		note_field = QWidget()
		note_field_layout = QHBoxLayout()
		# Create note QLabel and QLineEdit fields
		# and add them to note_field_layout
		note_label = QLabel("Note: ")
		self.line_edit = QLineEdit()
		self.line_edit.setMaxLength(50)
		self.line_edit.setText(trx['note'])
		note_field_layout.addWidget(note_label)
		note_field_layout.addWidget(self.line_edit)
		# Set layout for note_field
		note_field.setLayout(note_field_layout)

		# Create Save and Cancel buttons
		buttons = (
			QDialogButtonBox.StandardButton.Save |
			QDialogButtonBox.StandardButton.Cancel
			)
		
		# Create buttonBox and connect accept and reject signals
		self.buttonBox = QDialogButtonBox(buttons)
		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)
		
		# Create overall dialog layout
		dialog_layout = QVBoxLayout()

		# Add name and notes fields to dialog layout
		dialog_layout.addWidget(person_field)
		dialog_layout.addWidget(date_field)
		dialog_layout.addWidget(amount_field)
		dialog_layout.addWidget(note_field)
		dialog_layout.addWidget(self.buttonBox)
		
		# Set overall dialog layout
		self.setLayout(dialog_layout)


class DeleteTransactionDialog(QDialog):
	"""Window with checkboxes to select transactions to delete."""

	def __init__(self, main_window, person_id, parent=None):
		super().__init__(parent)

		self.setWindowTitle("Delete Transaction")
		self.setMinimumSize(500, 300)
		self.mw = main_window
		person = self.mw.db.people[person_id]['name']
		
		self.trx_list_view = QListView()
		self.item_model = QStandardItemModel()
		self.trx_list_view.setModel(self.item_model)
				
		# Create button box, then add "DELETE" and "Cancel" buttons
		self.buttonBox = QDialogButtonBox()
		self.buttonBox.addButton(
			"DELETE",
			QDialogButtonBox.ButtonRole.AcceptRole
			)
		self.buttonBox.addButton(QDialogButtonBox.StandardButton.Cancel)

		self.buttonBox.accepted.connect(self.accept)
		self.buttonBox.rejected.connect(self.reject)

		self.layout = QVBoxLayout()
		self.message = QLabel(
			f"Check each of {person.title()}'s transactions to delete:"
			)

		# Create list of person's transactions for use in QListView
		transactions = []
		for trx_id, info in self.mw.db.transactions.items():
			if info['person_id'] == person_id:
				trx_text = f"Trx# {trx_id} "
				trx_text += f"| {info['date']} "
				trx_text += f"| ${info['amount']} "
				if info['note']:
					trx_text += f" - {info['note']}"
				transactions.append(trx_text)

		for trx_text in transactions:
			item = QStandardItem(str(trx_text))
			item.setCheckable(True)
			item.setSelectable(False)
			item.setEditable(False)
			item.setDragEnabled(False)
			item.setDropEnabled(False)
			self.item_model.appendRow(item)

		self.warning = QLabel(
			"The selected transactions will be PERMANENTLY deleted."
			)

		self.layout.addWidget(self.message)
		self.layout.addWidget(self.trx_list_view)
		self.layout.addWidget(self.warning)
		self.layout.addWidget(self.buttonBox)

		self.setLayout(self.layout)