import json

from os import path as filePath

from PyQt6.QtCore import (
	QDate,
	Qt,
	)

class Database():
	
	def __init__(self):
		# Stores dictionary of people and associated transactions
		self.people = {}

		# Stores dictonary of transactions and their details
		self.transactions = {}

		# Create id_counter used to give all transactions unique IDs
		self.id_counter = 100

	# Load all data from JSON file
	def load_data(self):
		"""Load all people and transactions data from JSON file."""
		filename = 'ledger-data.json'

		if filePath.exists(filename):
			with open(filename) as f:
				loaded_json = json.load(f)
				data_dict = json.loads(loaded_json)
				self.people = data_dict['people']
				self.transactions = data_dict['transactions']
				self.id_counter = data_dict['id_counter']

			print("Data successfully loaded!")
			print("Updated data:")
			print(f"\tPeople: {self.people}")
			print(f"\tTransactions: {self.transactions}")
		
		else:
			self.save_data()

	
	def save_data(self):
		"""Save all people and transaction data to JSON file."""
		filename = 'ledger-data.json'
		data = {
				'people': self.people,
				'transactions': self.transactions,
				'id_counter': self.id_counter,
			}
		json_data = json.dumps(data, indent=4)

		with open(filename, 'w') as f:
			json.dump(json_data, f)

	def get_person_id(self, name):
		"""Takes a name and returns the matching id from the database."""
		person_id = 0
		
		for pid, info in self.people.items():
			if info['name'] == name:
				person_id = pid
		
		return person_id

	# Add person to self.people
	def add_person(self, name, notes=''):
		# Increase id counter by 1 and create person with resulting id.
		self.id_counter += 1
		new_person_id = str(self.id_counter)
		self.people[new_person_id]={
				"name": name.lower(),
				"notes": notes,
			}
	
	# Delete person from self.people
	def delete_person(self, name):
		del self.people[name]

	# Add transaction to self.transactions
	def add_transaction(self, transaction):
		self.id_counter += 1
		trx_id = str(self.id_counter)
		self.transactions[trx_id] = transaction
		# Transactions expected in the following format:
		# 	{id_number: {
		#		"person_id": person_id,
		#		"date": date,
		#		"amount": amount,
		#		"note": note,
		#	}}

	def edit_transaction(self, trx_id, edited_transaction):
		self.transactions.update({trx_id: edited_transaction})


	# Delete transactions from self.transactions
	def delete_transaction(self, trx_id):
		del self.transactions[trx_id]

