import os

from PyQt6.QtCore import QSize, Qt

from PyQt6.QtGui import (
	QFont,
	QStandardItem,
	QStandardItemModel,
	)

from PyQt6.QtWidgets import (
	QHBoxLayout,
	QLabel,
	QListView,
	QStackedLayout,
	QVBoxLayout,
	QWidget,
	)


class NameListView(QListView):
	"""Pane displaying names from database as clickable items."""
	def __init__(self, main_window):
		super().__init__()

		self.mw = main_window
		self.db = self.mw.db
		self.item_model = QStandardItemModel(self)
		self.setModel(self.item_model)
		self.clicked.connect(main_window.on_select_person)
		self.clicked.connect(self.on_select_person)
		self.selected_person = ''

	def update_name_list(self):
		# Clear all names from list and create people list
		self.item_model.clear()
		people = []

		# Check for people in database. If none, add "No people" to people list.
		if not self.db.people:
			people.append("No people")
		# If people in database, add each person's name to people list.
		else:
			for person_id, info in self.db.people.items():
				name = info['name']
				people.append(name)
			# Sort list of people alphabetically
			people.sort()
		# Add a name item to model for each person with 'notes' in tooltip.
		for name in people:
			standard_item = QStandardItem(name.title())
			standard_item.setEditable(False)
			standard_item.setDragEnabled(False)
			standard_item.setDropEnabled(False)
			if self.db.get_person_id(name):
				notes = self.db.people[self.db.get_person_id(name)]['notes']
				standard_item.setToolTip(notes)
			self.item_model.appendRow(standard_item)
		# Reselect person who was selected before name list was cleared.
		if self.selected_person:
			for row in range(self.item_model.rowCount()):
				index = self.item_model.index(row, 0)
				name = self.item_model.data(index, Qt.ItemDataRole.DisplayRole)
				if name.lower() == self.selected_person:
					self.setCurrentIndex(index)

	def on_select_person(self, index):
		person = self.item_model.item(index.row())
		person_name = person.text().lower()
		self.selected_person = person_name


class TransactionPane(QWidget):
	"""Pane housing the display stack with each person's transaction list."""

	def __init__(self, main_window):
		super().__init__()

		self.mw = main_window
		self.db = self.mw.db
		self.header_widget = QWidget()
		self.header_layout = QHBoxLayout()
		self.main_widget = QWidget()
		self.main_layout = QVBoxLayout()
		self.stacked_layout = QStackedLayout()

	def update_trx_stack(self):
		"""Updates transaction list view for each person in database."""
		widget_count = self.stacked_layout.count()
		widgets_to_remove = []
		
		for index in range(widget_count):
			widget = self.stacked_layout.widget(index)
			widgets_to_remove.append(widget)

		print(f"update_trx_stack() ran; widgets_to_remove = {widgets_to_remove}")

		for widget in widgets_to_remove:
			self.stacked_layout.removeWidget(widget)

		# If there are people in the database
		if self.db.people:
			# Add a transaction page for each person to the stacked_layout
			for person_id, info in self.db.people.items():
				person = info['name']
				trx_page = TransactionPage(self.mw, person)
				self.stacked_layout.addWidget(trx_page)
		# If there are no people in the database
		else:
			# Add a transaction page for "No People"
			person = "No people"
			trx_page = TransactionPage(self.mw, person)
			self.stacked_layout.addWidget(trx_page)
			
		self.setLayout(self.stacked_layout)


class TransactionPage(QWidget):
	"""Single page in the Transaction Pane stacked layout that houses 
	person name, person total, and transactions."""

	def __init__(self, main_window, person):
		super().__init__()

		self.mw = main_window
		self.db = main_window.db
		self.person = person
		self.page_layout = QVBoxLayout()
		self.header_widget = QWidget()
		self.header_layout = QVBoxLayout()
		
		self.name_label = QLabel(f"{person.title()}")
		name_font = QFont("Calibri", 18)
		self.name_label.setFont(name_font)
		self.name_label.setAlignment(Qt.AlignmentFlag['AlignLeft'])
		self.header_layout.addWidget(self.name_label)

		self.trx_total = 0
		self.trx_total_label = QLabel()
		trx_total_font = QFont("Calibri", 20)
		trx_total_font.setWeight(trx_total_font.Weight['Bold'])
		self.trx_total_label.setFont(trx_total_font)
		self.trx_total_label.setAlignment(Qt.AlignmentFlag['AlignLeft'])
		self.update_trx_total()
		self.header_layout.addWidget(self.trx_total_label)

		self.header_widget.setLayout(self.header_layout)
		self.header_widget.setFixedHeight(80)

		self.list_view = TransactionListView(self.mw, self.person)
		self.list_view.update_trx_list()

		self.page_layout.addWidget(self.header_widget, stretch=1)
		self.page_layout.addWidget(self.list_view, stretch=5)
		self.setLayout(self.page_layout)

	def update_trx_total(self):
		for transaction, info in self.db.transactions.items():
			if info['person_id'] == self.db.get_person_id(self.person):
				self.trx_total += info['amount']
		self.trx_total_label.setText(f"${self.trx_total}")


class TransactionListView(QListView):
	"""View in the Transaction Page that displays person's transaction list."""

	def __init__(self, main_window, person):
		super().__init__()

		# Initialize database, person, and item model, then set item model
		self.mw = main_window
		self.db = main_window.db
		self.person = person
		self.item_model = QStandardItemModel(self)
		self.setModel(self.item_model)

	def update_trx_list(self):
		"""Clear the transactions list and repopulate it from the database."""

		# Clear all transactions from item_model
		self.item_model.clear()
		# Create list to store transaction IDs
		transaction_ids = []

		# Add person's transaction IDs to transaction_ids list
		for transaction_id, info in self.db.transactions.items():
			if info['person_id'] == self.db.get_person_id(self.person):
				transaction_ids.append(transaction_id)
		
		if not transaction_ids:
			# If no transactions, add "No transactions" text to list
			name = self.person.title()
			standard_item = QStandardItem(f"No transactions")
			standard_item.setSelectable(False)
			standard_item.setEditable(False)
			standard_item.setDragEnabled(False)
			standard_item.setDropEnabled(False)
			self.item_model.appendRow(standard_item)
		else:
			# Add transactions to display_text in order of transaction ID
			for transaction_id in transaction_ids:
				transaction = self.db.transactions[transaction_id]
				display_text = f"Trx# {transaction_id} "
				display_text += f"| {transaction['date']} "
				display_text += f"| ${transaction['amount']} "
				if transaction['note']:
					display_text += f" - {transaction['note']}"

				# Create standard_item with display_text and add to item_model
				# so it displays in the TransactionListView list
				standard_item = QStandardItem(str(display_text))
				standard_item.setSelectable(True)
				standard_item.setEditable(False)
				standard_item.setDragEnabled(False)
				standard_item.setDropEnabled(False)
				self.item_model.appendRow(standard_item)


class TransactionsTotal(QWidget):
	"""Shows and updates the total of all transactions."""

	def __init__(self, main_window):
		super().__init__()

		self.mw = main_window
		self.db = main_window.db
		self.layout = QVBoxLayout()
		self.header = QLabel('Transactions Total:')
		self.total = QLabel('$0')
		# Set total font and align center
		font = QFont("Calibri", 26)
		font.setWeight(font.Weight['Bold'])
		self.total.setFont(font)
		self.total.setAlignment(Qt.AlignmentFlag['AlignCenter'])
		# Add header and total to layout
		self.layout.addWidget(self.header)
		self.layout.addWidget(self.total)
		self.setLayout(self.layout)

	def update_total(self):
		# Add all transaction amounts and update total label with new total
		new_total = 0
		for transaction, info in self.db.transactions.items():
			new_total += info['amount']
		self.total.setText(f"${new_total}")