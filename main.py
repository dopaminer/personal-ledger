import os, sys

from database import Database
from panes import (
	NameListView,
	TransactionPane,
	TransactionListView,
	TransactionsTotal
	)
from dialogs import (
	AddPersonDialog,
	EditPersonDialog,
	DeletePersonDialog,
	AddTransactionDialog,
	EditTransactionDialog,
	DeleteTransactionDialog,
	)

from PyQt6.QtCore import QSize, Qt
from PyQt6.QtGui import (
	QAction,
	QFont,
	QIcon,
	QKeySequence,
	QStandardItem,
	QStandardItemModel,
	)
from PyQt6.QtWidgets import (
	QApplication,
	QButtonGroup,
	QCheckBox,
	QComboBox,
	QDateEdit,
	QDialog,
	QDialogButtonBox,
	QDoubleSpinBox,
	QGroupBox,
	QHBoxLayout,
	QLabel,
	QLineEdit,
	QListView,
	QMainWindow,
	QMessageBox,
	QPlainTextEdit,
	QPushButton,
	QRadioButton,
	QSizePolicy,
	QStackedLayout,
	QToolBar,
	QVBoxLayout,
	QWidget,
	)


class MainWindow(QMainWindow):
	"""Class that controls the main application window."""
	def __init__(self):
		super(MainWindow, self).__init__()

		# Set main application window title
		self.setWindowTitle("Personal Ledger")
		# Create session database and load data from JSON file
		self.db = Database()
		self.db.load_data()
		# Create variable for application menu bar
		self.menu = self.menuBar()

		# Create layout for overall application window
		self.main_layout = QHBoxLayout()
		# Set margins for content inside main application window
		self.main_layout.setContentsMargins(5, 5, 5, 5)
		self.main_layout.setSpacing(1)

		# Create actions to be used in menu bar and toolbar
		self.create_actions()
		# Populate the menu bar with the created actions
		self.populate_menubar()
		# Create toolbar and add to main window
		self.toolbar = QToolBar("Main Toolbar")
		self.toolbar.setIconSize(QSize(16,16))
		self.addToolBar(self.toolbar)
		# Populate toolbar with the created actions
		self.populate_toolbar()

		# Create layouts for application window sections
		self.left_layout = QVBoxLayout()
		self.right_layout = QVBoxLayout()

		#=== LEFT LAYOUT ===
		# Create name list and add to left_layout
		self.name_list = NameListView(self)
		self.name_list.setFixedWidth(220)
		self.left_layout.addWidget(self.name_list, stretch=1)
		# Create transaction totals pane and add to left_layout
		self.trx_total_pane = TransactionsTotal(self)
		self.left_layout.addWidget(self.trx_total_pane)

		#=== RIGHT LAYOUT ===
		# Create transactions pane and add to right_layout
		self.trx_pane = TransactionPane(self)
		self.right_layout.addWidget(self.trx_pane, stretch=3)

		# Add left and right layouts to main layout
		self.main_layout.addLayout(self.left_layout)
		self.main_layout.addLayout(self.right_layout)
		# Create widget to contain main window layout and add to main window
		widget = QWidget()
		widget.setLayout(self.main_layout)
		self.setCentralWidget(widget)
		# Update all panes in main window
		self.update_all()

	def create_actions(self):
		"""Creates program actions to be used in the menu bar and toolbar."""
		# Create file actions
		self.exit = QAction("E&xit",self)
		# Create person actions
		self.add_person = QAction(QIcon("add-per.png"),"&Add Person",self)
		self.edit_person = QAction(QIcon("edit-per.png"),"&Edit Person",self)
		self.delete_person = QAction(QIcon("del-per.png"),"&Delete People",self)
		# Transaction actions
		self.add_trx = QAction(QIcon("add-trx.png"),"&Add Transaction",self)
		self.edit_trx = QAction(QIcon("edit-trx.png"),"&Edit Transaction",self)
		self.delete_trx = QAction(QIcon("del-trx.png"),"&Delete Transactions",self)

		# Connect file actions
		self.exit.triggered.connect(self.on_exit)
		# Connect person actions
		self.add_person.triggered.connect(self.on_add_person)
		self.edit_person.triggered.connect(self.on_edit_person)
		self.delete_person.triggered.connect(self.on_delete_person)
		# Connect transaction actions
		self.add_trx.triggered.connect(self.on_add_trx)
		self.edit_trx.triggered.connect(self.on_edit_trx)
		self.delete_trx.triggered.connect(self.on_del_trx)

	def populate_menubar(self):
		"""Adds actions to menu bar."""
		# Create top level menus
		file_menu = self.menu.addMenu("&File")
		per_menu = self.menu.addMenu("&People")
		trx_menu = self.menu.addMenu("&Transactions")

		# Populate file menu
		file_menu.addAction(self.exit)
		# Populate people menu
		per_menu.addAction(self.add_person)
		per_menu.addAction(self.edit_person)
		per_menu.addAction(self.delete_person)
		# Populate transactions menu
		trx_menu.addAction(self.add_trx)
		trx_menu.addAction(self.edit_trx)
		trx_menu.addAction(self.delete_trx)

	def populate_toolbar(self):
		"""Adds actions to toolbar."""
		self.toolbar.addAction(self.add_person)
		self.toolbar.addAction(self.edit_person)
		self.toolbar.addAction(self.delete_person)
		self.toolbar.addSeparator()
		self.toolbar.addAction(self.add_trx)
		self.toolbar.addAction(self.edit_trx)
		self.toolbar.addAction(self.delete_trx)

	def update_all(self):
		"""Update name pane, transactions pane, and transactions total."""
		self.name_list.update_name_list()
		self.trx_pane.update_trx_stack()
		self.trx_total_pane.update_total()

	def select_person(self, person):
		# Select transaction person in name list
		name_list_model = self.name_list.item_model
		for row in range(name_list_model.rowCount()):
			index = name_list_model.index(row, 0)
			name = name_list_model.data(index, Qt.ItemDataRole.DisplayRole)
			if name.lower() == person:
				self.name_list.setCurrentIndex(index)

		# Set transaction view to transaction person and deselect all
		# transactions from other views
		trx_pane_layout = self.trx_pane.stacked_layout
		trx_view_count = trx_pane_layout.count()
		for index in range(trx_view_count):
			page = trx_pane_layout.widget(index)
			view = page.list_view
			if person == view.person:
				trx_pane_layout.setCurrentIndex(index)
			else:
				view.clearSelection()
				view.model().layoutChanged.emit()
	
	def on_exit(self):
		self.db.save_data()
		sys.exit()

	def on_select_person(self, model_index):
		clicked_item = self.name_list.item_model.item(model_index.row())
		person = clicked_item.text().lower()
		transaction_pane_layout = self.trx_pane.stacked_layout
		transaction_view_count = transaction_pane_layout.count()

		for index in range(transaction_view_count):
			page = transaction_pane_layout.widget(index)
			if person == page.person:
				transaction_pane_layout.setCurrentIndex(index)

	def on_add_person(self, triggered):
		# Create Add Person dialog window
		dlg = AddPersonDialog(self)

		# If user clicks Save, process and save new person to database
		if dlg.exec():
			# Capture new name and create list of existing names
			new_name = dlg.name_line_edit.text().lower()
			existing_names = []
			for person, info in self.db.people.items():
				existing_names.append(info['name'])

			# Cancel new user creation if new_name is blank
			if new_name == '':
				name_blank_dialog = QMessageBox(
					QMessageBox.Icon.Warning,
					"No name entered",
					"You didn't enter a name,\nso no person will be added",
					QMessageBox.StandardButton.Ok,
					parent = self
					)
				name_blank_dialog.exec()
			
			# Cancel new user creation if new_name is "no person"
			elif new_name == 'no person' or new_name == 'no people':
				message = f'"{new_name}" is not a valid name. Try again.'
				invalid_name_dialog = QMessageBox(
					QMessageBox.Icon.Warning,
					"Invalid name",
					message,
					QMessageBox.StandardButton.Ok,
					parent = self
					)
				invalid_name_dialog.exec()

			# Cancel new user creation if name already in self.db.people
			elif new_name in existing_names:
				person_exists_dialog = QMessageBox(
					QMessageBox.Icon.Warning,
					"Person already exists",
					"This person already exists\nand will not be added",
					QMessageBox.StandardButton.Ok,
					parent = self
					)
				person_exists_dialog.exec()

			# If name not blank and not in database, add to database
			else:
				notes = dlg.notes_text_edit.document().toPlainText()
				self.db.add_person(new_name, notes)
				self.db.save_data()
				self.update_all()
				self.select_person(new_name)

		# If user cancels, close dialog
		else:
			print("Operation Canceled")

	def on_edit_person(self, triggered):
		# Get person's name from current selection in name list
		selection = self.name_list.currentIndex()
		name = self.name_list.item_model.data(selection).lower()

		# If no people, give message and exit edit operation
		if name == "no people":
			no_people_dialog = QMessageBox(
					QMessageBox.Icon.Warning,
					"No people to edit",
					"There are no people to edit.",
					QMessageBox.StandardButton.Ok,
					parent = self
					)
			no_people_dialog.exec()
		# If valid person selected, proceed
		else:
			# Get person's id by their name
			person_id = self.db.get_person_id(name)
			# Get person's notes from database
			notes = self.db.people[person_id]['notes']
			# Create Edit Person dialog window
			dlg = EditPersonDialog(self, name, notes, parent=self)

			if dlg.exec():
				# Update database with edited information
				new_name = dlg.name_line_edit.text().lower()
				new_notes = dlg.notes_text_edit.document().toPlainText()
				
				if name != new_name and self.db.get_person_id(new_name):
					# If person name already exists in database, cancel update
					person_exists_dialog = QMessageBox(
						QMessageBox.Icon.Warning,
						"Person already exists",
						"A person with this name already exists.\nUpdate canceled.",
						QMessageBox.StandardButton.Ok,
						parent = self
						)
					person_exists_dialog.exec()
				else:
					# If person name doesn't exist, update person
					person_to_update = self.db.people[person_id]
					person_to_update.update({
						'name': new_name,
						'notes': new_notes
						})
					# Save to file and update all panes
					self.db.save_data()
					self.update_all()
					# Select person from name list
					self.select_person(new_name)

	
	def on_delete_person(self, triggered):
		# Create Delete Person dialog window
		dlg = DeletePersonDialog(self, parent=self)
		
		# Handle "Confirm" button press
		if dlg.exec():			
			# Add all selected people's transactions to list for deletion
			transactions_to_delete = []
			people_to_delete = []
			for checkbox in dlg.people_checkboxes:
				if checkbox.isChecked():
					person = checkbox.text().lower()
					people_to_delete.append(person)
					for transaction, info in self.db.transactions.items():
						if info["person_id"] == self.db.get_person_id(person):
							transactions_to_delete.append(transaction)
			# Delete listed transactions from database
			for transaction in transactions_to_delete:
				del self.db.transactions[transaction]
			# Delete listed people from database
			for person in people_to_delete:
				del self.db.people[self.db.get_person_id(person)]

			# Update all panes in main window
			self.update_all()
			# Save changes to file
			self.db.save_data()
							
		else:
			print("Delete Person CANCELED!")

	def on_add_trx(self, triggered):
		# If no one in database, cancel operation
		if not self.db.people:
			no_people_dialog = QMessageBox(
				QMessageBox.Icon.Warning,
				"No people in database",
				"No people in database.\nAdd person before adding transaction.",
				QMessageBox.StandardButton.Ok,
				parent = self
				)
			no_people_dialog.exec()
		# If people in database, proceed
		else:
			# Get person's name from current selection in name list
			selection = self.name_list.currentIndex()
			person = self.name_list.item_model.data(selection).lower()
			person_id = self.db.get_person_id(person)
			# Create Add Transaction dialog window
			dlg = AddTransactionDialog(self, person_id, parent=self)

			# Handle "Save" button press
			if dlg.exec():
				print("Add Transaction dialog executed")

				# Transactions expected in the following format:
			# 	{id_number: {
			#		"person": person,
			#		"date": date,
			#		"amount": amount,
			#		"note": note,
			#	}}

				# Add dialog field data to transaction dictionary
				person = dlg.person_combo_box.currentText().lower()
				person_id = str(self.db.get_person_id(person))
				date = dlg.date_edit.date()
				date_string = date.toString(Qt.DateFormat.ISODate)
				amount = dlg.spin_box.value()
				note = dlg.line_edit.text()
				transaction = {
					'person_id': person_id,
					'date': date_string,
					'amount': amount,
					'note': note,
					}

				# Add transaction to database, save to file, update all panes.
				self.db.add_transaction(transaction)
				self.db.save_data()
				self.update_all()
				# Select person in name list
				self.select_person(person)

	def on_edit_trx(self, triggered):
		# Get current page from transaction pane
		trx_page_index = self.trx_pane.stacked_layout.currentIndex()
		trx_page = self.trx_pane.stacked_layout.widget(trx_page_index)
		trx_view = trx_page.list_view
		trx_model_index = trx_view.currentIndex()
		# Get text from transaction in item model
		trx_model = trx_view.item_model
		trx_text = trx_model.data(trx_model_index, Qt.ItemDataRole.DisplayRole)
		
		if trx_text and trx_text != "No transactions":
			# Get transaction id number from item text
			trx_text_split = trx_text.split(" ", 2)
			trx_id = trx_text_split[1]
			
			dlg = EditTransactionDialog(self, trx_id, parent=self)
			if dlg.exec():
				# Add dialog field data to edited_transaction dictionary
				person = dlg.person_combo_box.currentText().lower()
				person_id = str(self.db.get_person_id(person))
				date = dlg.date_edit.date()
				date_string = date.toString(Qt.DateFormat.ISODate)
				amount = dlg.spin_box.value()
				note = dlg.line_edit.text()
				edited_transaction = {
					'person_id': person_id,
					'date': date_string,
					'amount': amount,
					'note': note,
				}
				# Update transaction in database, save to file, update all panes
				self.db.edit_transaction(trx_id, edited_transaction)
				self.db.save_data()
				self.update_all()
				self.select_person(person)
			else:
				print("Edit Transaction Canceled!")

		else:
			no_trx_selected_dialog = QMessageBox(
						QMessageBox.Icon.Warning,
						"No transaction selected",
						("No transaction selected.\nSelect transaction "
							"and try again."),
						QMessageBox.StandardButton.Ok,
						parent = self
						)
			no_trx_selected_dialog.exec()

	def on_del_trx(self, triggered):
		# Check for people in database
		if not self.db.people:
			no_people_dialog = QMessageBox(
				QMessageBox.Icon.Warning,
				"No people in database",
				"No people in database.\nNo transactions to delete.",
				QMessageBox.StandardButton.Ok,
				parent = self
				)
			no_people_dialog.exec()

		else:
			# Get person's name from current selection in name list
			selection = self.name_list.currentIndex()
			person = self.name_list.item_model.data(selection).lower()
			person_id = self.db.get_person_id(person)
			# Create Delete Transaction dialog window
			dlg = DeleteTransactionDialog(self, person_id, parent=self)
			
			if dlg.exec():
				# Add all selected transactions to list for deletion
				trx_to_delete = []
				item_model = dlg.item_model
				for row in range(item_model.rowCount()):
					trx_item = item_model.item(row)
					if trx_item.checkState() == Qt.CheckState.Checked:
						index = item_model.index(row, 0)
						trx_text = item_model.data(index, Qt.ItemDataRole.DisplayRole)
						trx_text_split = trx_text.split(" ", 2)
						trx_id = trx_text_split[1]
						self.db.delete_transaction(trx_id)

				self.db.save_data()
				self.update_all()
				self.select_person(person)
			else:
				print("Delete Transactions Canceled!")


# Get path to directory main.py is in
app_path = os.path.dirname(__file__)
# Change directory to that path to ensure file references work
os.chdir(app_path)

app = QApplication(sys.argv)
w = MainWindow()
w.show()
app_font = QFont("Calibri", 11)
app.setFont(app_font)
app.exec()