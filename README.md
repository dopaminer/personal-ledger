# personal-ledger

Program to keep track of incoming and outgoing money for multiple people. Can create, edit, and remove people and transactions. Written in Python using PyQt.